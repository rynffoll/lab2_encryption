#include "Cipher.h"
#include <cassert>
#include <stdio.h>
#include <string>
#include<iostream>
#include<fstream>

using namespace std;

unsigned long long key;
unsigned long long key1;
unsigned long long key2;

void Crypt(string inputfileName, string outputFileName, int offset, bool crypt)
{
	fstream inputFile(inputfileName, ios::in || ios::binary);
	fstream outputFile(outputFileName, ios::out);
	auto cipher = new Cipher();
	unsigned long long originalData;
	char buf[8];
	int count = 0;

	inputFile.seekg(offset);
	while (!inputFile.eof())
	{
		inputFile.get(buf[count]);
		count++;
		if (count == 8)
		{
			originalData = *(long long *)& buf;

			if (crypt)
			{
				unsigned long long encryptedData = cipher->desxEncrypt(originalData, key, key1, key2);
				outputFile.write((char *)&encryptedData, 8);
			}
			else
			{
				
				unsigned long long decryptedData = cipher->desxDecrypt(originalData, key, key1, key2);
				outputFile.write((char *)&decryptedData, 8);
			}
			count = 0;
			memset(buf, ' ', 8);
		}
	}
	originalData = *(long long *)& buf;
	if (crypt)
	{
		unsigned long long encryptedData = cipher->desxEncrypt(originalData, key, key1, key2);
		outputFile.write((char *)&encryptedData, count - 1);
	}
	else
	{
		unsigned long long decryptedData = cipher->desxDecrypt(originalData, key, key1, key2);
		outputFile.write((char *)&decryptedData, count - 1);
	}
};

int main()
{
	char strkey[7];
	char strkey1[8];
	char strkey2[8];

	fstream fileStream("input.txt", ios_base::in | ios_base::out | ios_base::binary);
	fileStream.read((char *)&strkey, sizeof strkey);
	fileStream.read((char *)&strkey1, sizeof strkey1);
	fileStream.read((char *)&strkey2, sizeof strkey2);

	unsigned long long key = * (long long *)&strkey;
	unsigned long long key1 = * (long long *)&strkey1;
	unsigned long long key2 = * (long long *)&strkey2;
	

	Crypt("input.txt", "encrypted.txt", 23, true);
	Crypt("encrypted.txt", "decrypted.txt", 0, false);

	return 0;
}