#include "Cipher.h"
#include <utility>
#include <vector>

Cipher::Cipher()
{
}

Cipher::~Cipher()
{
}

unsigned long long Cipher::ip(unsigned long long data)
{
	unsigned long long result = 0;
	for (int i = 0; i < 64; ++i){
		result |= ((data >> (tableIp[i] - 1)) & 1ULL) << i;
	}
	return result;
}

unsigned long long Cipher::ip1(unsigned long long data)
{
	unsigned long long result = 0;
	for (int i = 0; i < 64; ++i)
	{
		result |= ((data >> (tableIp1[i] - 1)) & 1ULL) << i;
	}
	return result;
}

unsigned long long Cipher::desxEncrypt(unsigned long long data, unsigned long long key,
	unsigned long long key1, unsigned long long key2)
{
	return key2^desEncrypt(data^key1, key);
}

unsigned long long Cipher::desxDecrypt(unsigned long long data, unsigned long long key,
	unsigned long long key1, unsigned long long key2)
{
	return key1^desDecrypt(data^key2, key);
}

unsigned long long Cipher::e(unsigned long long data)
{
	unsigned long long result = 0;
	for (int i = 0; i < 48; ++i)
	{
		result |= ((data >> (tableE[i] - 1)) & 1ULL) << i;
	}
	return result;
}

unsigned long long Cipher::s(unsigned long long data)
{
	unsigned long long result = 0;
	for (int i = 0; i < 8; ++i)
	{
		unsigned long long bi = (data >> (i * 6)) & ((1 << 6) - 1);
		unsigned long long a = (((bi >> 5) & 1) << 1) | (bi & 1);
		unsigned long long b = (bi >> 1) & ((1 << 4) - 1);
		result |= tableS[i][a][b] << (i * 4);
	}
	return result;
}

unsigned long long Cipher::p(unsigned long long data)
{
	unsigned long long result = 0;
	for (int i = 0; i < 32; ++i)
	{
		result |= ((data >> (tableP[i] - 1)) & 1ULL) << i;
	}
	return result;
}

unsigned long long Cipher::desEncrypt(unsigned long long data, unsigned long long key)
{
	auto keys = getKeys(key);
	data = ip(data);
	unsigned long long l = data >> 32, r = data << 32 >> 32;
	for (int i = 0; i < 16; ++i)
	{
		std::swap(l, r);
		r = r^f(l, keys[i]);
	}
	data = (l << 32) | r;
	data = ip1(data);
	return data;
}

unsigned long long Cipher::desDecrypt(unsigned long long data, unsigned long long key)
{
	auto keys = getKeys(key);
	data = ip(data);
	unsigned long long l = data >> 32, r = data << 32 >> 32;
	for (int i = 15; i >= 0; --i)
	{

		std::swap(l, r);
		l = l^f(r, keys[i]);
	}
	data = (l << 32) | r;
	data = ip1(data);
	return data;
}

unsigned long long Cipher::f(unsigned long long data, unsigned long long key)
{
	data = e(data);
	data ^= key;
	data = s(data);
	data = p(data);
	return data;
}

std::vector <unsigned long long> Cipher::getKeys(unsigned long long key)
{
	std::vector<unsigned long long> result;
	int shift = 0;
	for (int i = 0; i < 16; ++i)
	{
		shift += tableShift[i];
		unsigned long long c = 0ULL, d = 0ULL;
		for (int j = 0; j < 28; ++j)
		{
			int k = (j + 28 - shift) % 28;
			c |= ((key >> (tableC[k] - tableC[k] / 8 - 1)) & 1ULL) << j;
			d |= ((key >> (tableD[k] - tableD[k] / 8 - 1)) & 1ULL) << j;
		}
		unsigned long long cd = (c << 28) | d;
		unsigned long long res = 0ULL;
		for (int j = 0; j < 48; ++j)
		{
			res |= ((cd >> (tableCD[j] - 1)) & 1ULL) << j;
		}
		result.push_back(res);
	}
	return result;
}